#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
void phelp ();
int main (int argc, char** argv){
 if (argc==1){ phelp (); return 1; }
 int start= argc>=3? atoi (argv[1]): 1;
 int target= atoi (argc>=4? argv[3]: argv[argc-1]);
 int padding= argc==5? atoi (argv[4]): 0;
 int step= argc>=4? atoi (argv[2]): 1;
 for (; start<=target; start+=step)
  printf ("%0.*2$d\n", start, padding);
 return 0;
}
static char* helpmessage=
 "iseq [start] [step] target [padding] :\n\n"
 " iseq will function much like seq does with the\n"
 " exception of its argument format, restricted\n"
 " options and it being integer only. All arguments\n"
 " have a set order they can be in. 'Target' must\n"
 " always be defined last and each optional argument\n"
 " must be added to the last, from left to right.\n"
 " You cannot define 'padding' without 'target',\n"
 " 'step', or start. You cannot define 'step'\n"
 " without 'start', you must define 'start' before\n"
 " 'target' and you must always define 'target'\n\n"

 " Here are the definitions of each argument:\n"
 " start:      Range start\n"
 " target:     Range end\n"
 " padding:    Amount of zeros to pad\n"
 " step:       Incremention rate every iteration\n\0";
void phelp (){ write (1, helpmessage, strlen (helpmessage)); }
