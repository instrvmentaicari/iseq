warnings=-Wno-format-invalid-specifier -Wno-format-extra-args
compiler=clang
cflags=-O2 -mavx2 -mtune=intel
iseq: iseq.c
	$(compiler) ./iseq.c -o ./iseq $(warnings) $(cflags)
nasm: iseq.a
	nasm iseq.a -o ./iseq
dumpasm:
	$(compiler) ./iseq.c -S -masm=intel -o ./iseq.s $(warnings) $(cflags)
install: iseq
	sudo cp ./iseq /usr/bin
	@echo
	@echo "Invoke iseq to get usage"
uninstall:
	sudo rm /usr/bin/iseq
clean:
	rm ./iseq
