<<[[ iseq ]]>>

 This is a very simple alternative to seq that has structured
 arguments ( not free form ) in 20 or so functional lines of
 C. Made because normal seq doesn't really follow my
 understanding of unix philosophy and is needlessly
 complicated to learn.

To install:
 sudo make install

To use:
 Invoke with no arguments for help message.
